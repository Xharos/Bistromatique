package fr.xharos.bistromatique.parameter.symbol;

import fr.xharos.bistromatique.functional.Validator;
import java.util.ArrayList;
import java.util.List;

/**
 * File <b>SymbolValidator</b> located on fr.xharos.bistromatique.parameter.symbol
 * SymbolValidator is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 17:29
 * @since 1.0.0
 */
public class SymbolValidator implements Validator<Boolean, String> {

    /*
    Ordered default mathematical symbol :
    + * - / ^ % ( )
     */

    private final List<SymbolToken> symbolTokenList;

    public SymbolValidator() {
        this.symbolTokenList = new ArrayList<>();
    }

    @Override
    public Boolean validate(String param) {
        if (param == null || param.isEmpty())
            throw new UnsupportedOperationException("Symbol parameter is null or empty!");
        System.out.println("-- Start symbol registration -- ");
        int paramLength = param.length();
        if (paramLength != SymbolType.values().length)
            throw new UnsupportedOperationException("Symbol parameter does not contains " + SymbolType.values().length + " symbols!");
        for (int i = 0; i < paramLength; i++) {
            String value = String.valueOf(param.charAt(i));
            SymbolToken token = new SymbolToken(SymbolType.getType(i), value);
            if (!valueExist(token)) {
                symbolTokenList.add(token);
                System.out.println("Register symbol " + token.getType() + " with value " + token.getValue());
            } else
                throw new UnsupportedOperationException("Value " + token.getValue() + " is already defined for another symbol!");
        }
        System.out.println();
        return true;
    }

    public List<SymbolToken> getSymbolTokenList() {
        return symbolTokenList;
    }

    private boolean valueExist(SymbolToken token) {
        return symbolTokenList.stream().anyMatch((tok) -> tok.getValue().equalsIgnoreCase(token.getValue()));
    }
}
