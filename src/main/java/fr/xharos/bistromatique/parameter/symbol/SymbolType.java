package fr.xharos.bistromatique.parameter.symbol;

/**
 * File <b>SymbolType</b> located on fr.xharos.bistromatique.parameter.symbol
 * SymbolType is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 17:36
 * @since 1.0.0
 */
public enum SymbolType {

    /*
    TODO check with dermen if token level is correct
    - https://en.wikipedia.org/wiki/Shunting-yard_algorithm#Detailed_example
     */

    ADD((byte) 0, 2, false),
    MULTIPLY((byte) 1, 3, false),
    SOUSTRACT((byte) 2, 2, false),
    DIVIDE((byte) 3, 3, false),

    POW((byte) 4, 4, true),
    MOD((byte) 5, 4, false),

    OPEN_BRACKET((byte) 6, 5, false),
    CLOSE_BRACKET((byte) 7, 6, false);

    private byte    order;
    private int     level;
    private boolean right;

    SymbolType(byte order, int level, boolean right) {
        this.order = order;
        this.level = level;
        this.right = right;
    }

    public static SymbolType getType(int current) {
        for (SymbolType symbolType : SymbolType.values()) {
            if (symbolType.order == current)
                return symbolType;
        }
        return null;
    }

    public boolean isRightAssociativity() {
        return right;
    }

    public int getLevel() {
        return level;
    }
}
