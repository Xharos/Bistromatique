package fr.xharos.bistromatique.parameter.expression;

import fr.xharos.bistromatique.BistroParameter;
import fr.xharos.bistromatique.functional.Validator;

/**
 * File <b>ExpressionValidator</b> located on fr.xharos.bistromatique.parameter.expression
 * ExpressionValidator is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 18:07
 * @since 1.0.0
 */
public class ExpressionValidator implements Validator<Boolean, String> {

    private final BistroParameter parameter;

    public ExpressionValidator(BistroParameter parameter) {
        this.parameter = parameter;
    }

    @Override
    public Boolean validate(String param) throws UnsupportedOperationException {
        if (param == null || param.isEmpty())
            return false;
        System.out.println("-- Start expression validation -- ");
        for (int i = 0; i < param.length();i++) {
            String value = String.valueOf(param.charAt(i));
            if (!isValid(value))
                throw new UnsupportedOperationException("Value " + value + " is not defined!");
        }
        System.out.println("Done\n");
        return true;
    }

    private boolean isValid(String value) {
        return parameter.getBase().contains(value) ||
                parameter.getSymbol().getSymbolTokenList().stream().anyMatch(token -> token.getValue().equalsIgnoreCase(value));
    }
}
