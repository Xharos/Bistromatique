package fr.xharos.bistromatique.parameter.expression;

import fr.xharos.bistromatique.BistroParameter;
import fr.xharos.bistromatique.token.Token;
import fr.xharos.bistromatique.utils.StringUtils;

/**
 * File <b>ExpressionToken</b> located on fr.xharos.bistromatique.parameter.expression
 * ExpressionToken is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 19:12
 * @since 1.0.0
 */
public class ExpressionToken extends Token {

    private static final String INPUT_BASE  = BistroParameter.getInstance().getBase();
    private static final String BINARY_BASE = "01";
    private final String originalValue;//TODO remove, only for debug

    public ExpressionToken(String value, BistroParameter parameter) {
        super(value, 1);
        this.originalValue = value;
    }

    public void convertToBinary() {
        updateValue(StringUtils.stringToBase(value, INPUT_BASE, BINARY_BASE));
    }

}
