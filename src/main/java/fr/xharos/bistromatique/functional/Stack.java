package fr.xharos.bistromatique.functional;

import java.util.function.Consumer;

/**
 * File <b>Stack</b> located on fr.xharos.bistromatique.functional
 * Stack is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 19/07/2017 at 19:00
 * @since 1.0.0
 */
public interface Stack<T> {

    /**
     * Push this value to the top of the stack
     *
     * @param object a value to push
     */
    void push(T object);

    /**
     * @return the current size of this stack
     */
    int size();

    /**
     * Get the object store at the given index
     *
     * @param index the current index
     * @return an object, or else throw an {@link ArrayIndexOutOfBoundsException}
     */
    T get(int index);

    /**
     * Perform a get, and then delete the value stored at the given index
     *
     * @param index the current index
     * @return this object if exist, or else throw an {@link ArrayIndexOutOfBoundsException}
     */
    T pop(int index);

    /**
     * Pop the last element of the stack if {@link #size()} != 0
     *
     * @return this object if exist, or else throw an {@link ArrayIndexOutOfBoundsException}
     * @see #pop(int)
     */
    T popLast();

    /**
     * Perform an operation on each element of the stack
     *
     * @param action an action to execute
     */
    void forEach(Consumer<? super T> action);

}
