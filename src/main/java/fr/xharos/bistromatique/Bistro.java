package fr.xharos.bistromatique;

import fr.xharos.bistromatique.parser.ExpressionParser;

/**
 * File <b>Bistro</b> located on fr.xharos.bistromatique
 * Bistro is a part of Bistro.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 17:13
 * @since 1.0.0
 */
public class Bistro {

    private final BistroParameter parameter;

    public Bistro(BistroParameter parameter) {
        this.parameter = parameter;
    }

    public void validateInputArgs() throws UnsupportedOperationException {
        parameter.validateInputArgs();
    }

    public int getBaseSize() {
        return parameter.getBase().length();
    }

    public String getBase() {
        return parameter.getBase();
    }

    public String getExpression() {
        return parameter.getExpression();
    }

    public void parse() throws Exception {
        ExpressionParser parser = new ExpressionParser(parameter.getExpression(), parameter);
        parser.parse();
    }
}
