package fr.xharos.bistromatique.utils;

/**
 * File <b>StringUtils</b> located on fr.xharos.bistromatique.utils
 * StringUtils is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 19/07/2017 at 18:55
 */
public class StringUtils {

    private static int stringToIntBase(String s, String base) {
        int n = 0;
        int i = 0;
        int sign = 1;
        int base_len = base.length();

        if (i < s.length() && (s.charAt(i) == '+' || s.charAt(i) == '-'))
            sign = s.charAt(i++) == '-' ? -1 : 1;
        while (i < s.length())
            n = n * base_len + base.indexOf(s.charAt(i++));
        return n * sign;
    }

    private static String intToStringBase(int n, String base) {
        int div = 1;
        int len = base.length();
        String s = "";

        if (n < 0)
            s += '-';
        else
            n *= -1;
        while (n / div <= -len)
            div *= len;
        while (div > 0) {
            s += base.charAt(-((n / div) % len));
            div /= len;
        }
        return s;
    }

    public static String stringToBase(String s, String inBase, String outBase) {
        return intToStringBase(stringToIntBase(s, inBase), outBase);
    }

}
