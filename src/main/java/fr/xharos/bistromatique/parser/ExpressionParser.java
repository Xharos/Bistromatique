package fr.xharos.bistromatique.parser;

import fr.xharos.bistromatique.BistroParameter;
import fr.xharos.bistromatique.parameter.expression.ExpressionToken;
import fr.xharos.bistromatique.parameter.symbol.SymbolToken;
import fr.xharos.bistromatique.parameter.symbol.SymbolType;
import fr.xharos.bistromatique.token.Token;
import fr.xharos.bistromatique.token.TokenStack;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * File <b>ExpressionParser</b> located on fr.xharos.bistromatique.parser
 * ExpressionParser is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 18:59
 * @since 1.0.0
 */
public class ExpressionParser {

    private final String          expression;
    private final BistroParameter parameter;

    public ExpressionParser(String expression, BistroParameter parameter) {
        this.expression = expression;
        this.parameter = parameter;
    }

    /**
     * Level 1 represent a number
     * Level 2 represent an add or a soustract
     * Level 3 represent a multiply or a divide
     * Level 4 represent a pow or a mod
     * Level 5 represent open bracket
     * Level 6 represent close bracket
     */
    public void parse() {
        List<Token> tokens = getTokens();
        TokenStack<Token> outputStack = new TokenStack();
        TokenStack<SymbolToken> operatorStack = new TokenStack();
        for (int i = 0; i < tokens.size(); i++) {
            Token token = tokens.get(i);
            int level = token.getLevel();
            if (level == 1)
                outputStack.push(token);
            else {
                SymbolToken symbolToken = (SymbolToken) token;
                if (symbolToken.getType() == SymbolType.OPEN_BRACKET) {
                    operatorStack.push(symbolToken);
                } else if (symbolToken.getType() == SymbolType.CLOSE_BRACKET) {
                    while (operatorStack.size() != 0) {
                        SymbolToken lastOperator = operatorStack.get(operatorStack.size() - 1);
                        if (lastOperator.getType() != SymbolType.OPEN_BRACKET)
                            outputStack.push(operatorStack.popLast());
                        else
                            break;
                    }
                    if (operatorStack.size() == 0)
                        throw new NumberFormatException("Cannot find at least one open bracket");
                    operatorStack.popLast();
                } else {
                    while (operatorStack.size() != 0) {
                        SymbolToken operator = operatorStack.get(operatorStack.size() - 1);
                        if (operator.getLevel() >= symbolToken.getLevel()) {
                            if (operator.getType() == SymbolType.OPEN_BRACKET || operator.getType() == SymbolType.CLOSE_BRACKET)
                                continue;
                            outputStack.push(operatorStack.popLast());
                        } else
                            break;
                    }
                }
            }
        }
        while (operatorStack.size() != 0) {
            SymbolToken operator = operatorStack.popLast();
            if (operator.getType() == SymbolType.OPEN_BRACKET || operator.getType() == SymbolType.CLOSE_BRACKET)
                throw new NumberFormatException("wtf");
            outputStack.push(operator);
        }
        final StringBuilder outputString = new StringBuilder();
        outputStack.forEach(token -> {
            outputString.append(token.getValue()).append("|");
            System.out.println("Token " + token.getValue() + " with level " + token.getLevel());
        });
        System.out.println(outputString.toString());
    }

    private List<Token> getTokens() {
        List<Token> tokens = new ArrayList<>();
        char[] c = expression.toCharArray();
        for (int i = 0; i < expression.length(); i++) {
            //Check if this char is a symbol
            Optional<SymbolToken> optSymbol = getSymbol(String.valueOf(c[i]));
            if (optSymbol.isPresent()) {
                tokens.add(optSymbol.get());
                continue;
            }
            //Or else, get the number
            ExpressionTokenWrapper wrapper = getNumber(c, i);
            i = wrapper.newPos - 1;

            wrapper.token.convertToBinary();

            tokens.add(wrapper.token);
        }
        return tokens;
    }

    private Optional<SymbolToken> getSymbol(String value) {
        return Optional.ofNullable(parameter.getSymbol().getSymbolTokenList().stream().filter(tok -> tok.getValue().equalsIgnoreCase(value)).findFirst().orElse(null));
    }

    private ExpressionTokenWrapper getNumber(char[] expression, int currentPos) {
        StringBuilder builder = new StringBuilder();
        while (currentPos <= (expression.length - 1)) {
            if (!getSymbol(String.valueOf(expression[currentPos])).isPresent()) {
                builder.append(expression[currentPos]);
                currentPos++;
            } else
                break;
        }
        return new ExpressionTokenWrapper(currentPos, new ExpressionToken(builder.toString(), parameter));
    }

    private class ExpressionTokenWrapper {

        private final int             newPos;
        private final ExpressionToken token;

        public ExpressionTokenWrapper(int newPos, ExpressionToken token) {
            this.newPos = newPos;
            this.token = token;
        }
    }
}
