package fr.xharos.bistromatique.bootsrap;

import fr.xharos.bistromatique.Bistro;
import fr.xharos.bistromatique.BistroParameter;

/**
 * File <b>BistroBootsrap</b> located on fr.xharos.bistromatique.bootsrap
 * BistroBootsrap is a part of Bistro.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 08/07/2017 at 17:13
 * @since 1.0.0
 */
public class BistroBootsrap {


    /**
     * Main method, launch this bistromatique
     * Sintax example :
     * - 0123456789  +*-/^%()   1+2-3^2/8
     * - xharos      +*-/^%()   x*h-a%r/(o-s^h)
     *
     * @param args who have to follow this sintax :
     *             base symbol expression
     */
    public static void main(String[] args) {
        if (args.length == 3) {
            Bistro bistro = new Bistro(new BistroParameter(args));
            try {
                bistro.validateInputArgs();
                System.out.println("-- Start parsing expression --");
                System.out.println(" - Base size  = " + bistro.getBaseSize());
                System.out.println(" - Base value  = " + bistro.getBase());
                System.out.println(" - Expression = " + bistro.getExpression());
                System.out.println(" - Binary conversion = " + true);
                System.out.println();
                bistro.parse();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        } else
            throw new UnsupportedOperationException("use the following pattern -> base symbol expression");
    }
}
