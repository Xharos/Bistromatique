package fr.xharos.bistromatique.token;

import fr.xharos.bistromatique.functional.Stack;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * File <b>TokenStack</b> located on fr.xharos.bistromatique.token
 * TokenStack is a part of Bistromatique.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Bistromatique is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 19/07/2017 at 19:01
 * @since 1.0.0
 */
public class TokenStack<T> implements Stack<T> {

    private final List<T> stack;

    public TokenStack() {
        this.stack = new ArrayList<>();
    }

    @Override
    public void push(T token) {
        stack.add(token);
    }

    @Override
    public int size() {
        return stack.size();
    }

    @Override
    public T get(int index) {
        return stack.get(index);
    }

    @Override
    public T pop(int index) {
        return stack.remove(index);
    }

    @Override
    public T popLast() {
        return stack.remove(size() - 1);
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        stack.forEach(action);
    }
}
